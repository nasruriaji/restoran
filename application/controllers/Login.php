<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index(){
		if($this->session->userdata('logged_in') == 'YES'){
			//$this->load->view('dashboard');
			redirect('dashboard');
		}else{
			$this->load->view('login');	
		}
	}

    public function do_login(){
		$user    	= $this->input->post("user");
		$password 	= md5($this->input->post("password"));
        
        //die($user.'========'.$password);
		$check_login = $this->m_dashboard->check_user(array("admin_username"=>$user, "admin_password"=>$password));
		if($check_login->num_rows() > 0) {
			$set_session = array(
				'logged_in'   => 'YES',
				'user_id'     => $check_login->row()->karyawan_id,
				'user_name'   => $check_login->row()->karyawan_username,
				'access_code' => $check_login->row()->karyawan_golongan,
                'date_in'     => date("Y-m-d")
			);
			$this->session->set_userdata( $set_session );
			redirect('dashboard');
		}
		else {
			redirect('login');
		}
    }
    public function logout(){
		$this->session->sess_destroy();
		redirect("login");
    }    
}
