<script type="text/javascript">
    $(document).ready(function(){

        $("#example1").DataTable({"ordering": false});

        $("#new_makanan").click(function(){
            link = "<?php echo site_url('master/makanan/insert'); ?>";
            load_modal({size:"modal-sm", header:"Buat Makanan Baru", url:link});
        });

    	

        $(".edit_makanan").click(function(){
            id   = $(this).attr("val-id");
            link = "<?php echo site_url('master/makanan/edit'); ?>/"+id;

            load_modal({size:"modal-sm",header:"Edit Makanan", url:link});
        });

       $(".delete_makanan").click(function(){
        	name = $(this).attr("val-name");
        	if(confirm("Yakin ingin menghapus makanan "+name+" ?")){
        		var location = $(this).attr("href");

				$.post( location, { load_template: "false" } ).done(function( data ) {
                    res=jQuery.parseJSON(data);
                    $("#panel_body").html(res.data);
				});
        	}
        	return false;
        });
    });
</script>


<!-- Content Header (Page header) -->
<!--section class="content-header">
    <h1 id="title_header">
        <?php if(isset($title)){echo $title;}else{echo "MAKANAN";} ?>
    </h1>
</section-->

<?php echo $this->session->flashdata('report'); ?>

<!-- Main content -->
<section class="content">
    <div class="box box-info">
        
        <div class="box-header">
            <div class="row" style="padding:0px 12px 10px">   
                <h3 class="pull-left" id="title_header" style="margin: 0px;">Makanan</h3>
                <button class="btn btn-info pull-right" id="new_makanan"><i class="fa fa-plus"></i> Buat Makanan Baru</button>
            </div>    
        </div><!-- /.box-header -->

        <div class="box-body">
            <table id="example1" class="table table-bordered table-striped table-hover">
                <thead>
                    <tr>               
                        <th>Nama Makanan</th>
                        <th>Jenis</th>
                        <th>Harga</th>
                        <th>Status</th>
                        
                        <th width="8%">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        foreach ($list->result() as $row) { 
                     ?>       
                            <tr>
                                <td><?php echo $row->makanan_name; ?></td>
                                <td><?php echo $row->makanan_jenis; ?></td>
                                <td><?php echo $row->makanan_harga; ?></td>
                                <td><?php echo $row->makanan_status; ?></td>

                                <td>
                                    <button class="btn btn-xs btn-warning btn-flat edit_makanan" val-id="<?php echo $row->makanan_id; ?>"><i class="fa fa-edit"></i></button>    

                                    <!--a href="<?php //echo site_url('master/makanan/delete').'/'.$row->makanan_id; ?>" class="delete_makanan" val-name="<?php echo $row->makanan_name; ?>">
                                        <button class="btn btn-xs btn-danger btn-flat"><i class="fa fa-trash"></i></button>
                                    </a-->                                  
                                </td>
                            </tr>

                           
                        <?php 
                        } // end foreach
                    ?>
                </tbody>    
            </table>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
</section><!-- /.content -->

