<script type="text/javascript">
    $(document).ready(function() {
        $("#insertbayar").bind('submit', function(event){
            //event.preventDefault();
            var link = $(this).attr('action');
            $.ajax({
                url: link,
                type: "POST",
                data: $(this).serialize(),
                cache: false,
                success: function(respon) {
                    res=jQuery.parseJSON(respon);
                    $("#panel-body").html(res.data);
                    $('#myModal').modal('hide');
                },
                error:function(respon){
                    $('#myModal').modal('hide');
                }
            });
            return false;
        });
    });
</script>


<?php echo form_open('master/bayar/post_insert', array('id'=>'insertbayar')); ?>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Nama makanan</label>
                    <input type="text" name="name" class="form-control" placeholder="Nama Makanan" required>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label>Jenis Makanan</label>
                    <select name="jenis" class="form-control">                      
                        <option value="makanan">Makanan</option>
                        <option value="minuman">Minuman</option>
                    </select>                       
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Harga</label>
                    <input type="text" name="harga" class="form-control" placeholder="Harga Makanan" required>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Status</label>
                    <select name="status" class="form-control">                      
                        <option value="ada">Ada</option>
                        <option value="kosong">Kosong</option>
                    </select>                      
                </div>
            </div>
        </div>
    </div>

    <div class="modal-footer"> 
        <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button> 
        <button type="submit" class="btn btn-primary">Simpan</button>
    </div>
<?php echo form_close(); ?>