<script type="text/javascript">
    $(document).ready(function(){

        $("#example1").DataTable({"ordering": false});

        $('input.bayar').click(function(e){
            if ($(this).is(':checked') ){
                //alert('No checked');
                var pesan_id=$(this).attr("val-id");
                var link = "<?php echo site_url('master/bayar/post_insert'); ?>";
                $.post( link, { pesan_id: pesan_id } ).done(function( respon )  {
                    res=jQuery.parseJSON(respon);
                    $("#panel-body").html(res.data);
                });                 
            }else{
                e.preventDefault();
                alert('Status pembayaran tidak boleh di ubah');
            }


        });

    });
</script>


<!-- Content Header (Page header) -->
<!--section class="content-header">
    <h1 id="title_header">
        <?php if(isset($title)){echo $title;}else{echo "PEMBAYARAN";} ?>
    </h1>
</section-->

<?php echo $this->session->flashdata('report'); ?>

<!-- Main content -->
<section class="content">
    <div class="box box-info">
        
        <div class="box-header">
            <div class="row" style="padding:0px 12px 10px">   
                <h3 class="pull-left" id="title_header" style="margin: 0px;">Pembayaran</h3>
                <!--button class="btn btn-info pull-right" id="new_bayar"><i class="fa fa-plus"></i> Buat Pembayaran Baru</button-->
            </div>    
        </div><!-- /.box-header -->



        <div class="box-body">
            <table id="example1" class="table table-bordered table-striped table-hover">
                <thead>
                    <tr>               
                        <th>Nama Makanan</th>
                        <th>Meja</th>
                        <th>Jumlah</th>
                        <th>harga</th>
                        <th>Total</th>
                        
                        <th width="8%">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        foreach ($list->result() as $row) { 
                     ?>       
                            <tr>
                                <td><?php echo $row->makanan_name; ?></td>
                                <td><?php echo $row->pemesanan_meja; ?></td>
                                <td><?php echo $row->pemesanan_jum; ?></td>
                                <td><?php echo $row->makanan_harga; ?></td>
                                <td><?php echo $row->total; ?></td>
                                <td>                                
                                    <label class="switch">
                                      <input class="bayar" type="checkbox" <?php if ($row->status=='1') echo "checked";?> val-id="<?php echo $row->pemesanan_id; ?>">
                                      <span class="slider round"></span>
                                    </label>
                                </td>
                            </tr>

                           
                        <?php 
                        } // end foreach
                    ?>
                </tbody>    
            </table>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
</section><!-- /.content -->

<style type="text/css">
  .switch {
    position: relative;
    display: inline-block;
    width: 60px;
    height: 34px;
  }

  .switch input {display:none;}

  .slider {
    position: absolute;
    cursor: pointer;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-color: #ccc;
    -webkit-transition: .4s;
    transition: .4s;
  }

  .slider:before {
    position: absolute;
    content: "";
    height: 26px;
    width: 26px;
    left: 4px;
    bottom: 4px;
    background-color: white;
    -webkit-transition: .4s;
    transition: .4s;
  }

  input:checked + .slider {
    background-color: #2196F3;
  }

  input:focus + .slider {
    box-shadow: 0 0 1px #2196F3;
  }

  input:checked + .slider:before {
    -webkit-transform: translateX(26px);
    -ms-transform: translateX(26px);
    transform: translateX(26px);
  }

    /* Rounded sliders */
  .slider.round {
    border-radius: 34px;
  }

  .slider.round:before {
    border-radius: 50%;
  }

</style>

