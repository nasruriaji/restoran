<script>
    $(document).ready(function() 
    {
        $("#editmakanan").bind('submit', function(event)
        {
            var link = $(this).attr('action');
            $.ajax({
                url: link,
                type: "POST",
                data: $(this).serialize(),
                cache: false,
                success: function(respon) {
                    res=jQuery.parseJSON(respon);
                    $("#panel-body").html(res.data);

                    $('#myModal').modal('hide');
                },
                error:function(respon){
                    $('#myModal').modal('hide');
                }
            });
            return false;
        });
    });
</script>


<?php echo form_open('master/makanan/post_edit', array('id'=>'editmakanan')); ?>
    <input type="hidden" name="makanan_id" value="<?php echo $data->makanan_id; ?>">
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Nama makanan</label>
                    <input type="text" name="name" class="form-control" value="<?php echo $data->makanan_name; ?>" required>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label>Jenis Makanan</label>
                    <select name="jenis" class="form-control">                      
                        <option value="makanan" <?php if($data->makanan_jenis=='makanan') echo "selected"; ?>>Makanan</option>
                        <option value="minuman" <?php if($data->makanan_jenis=='minuman') echo "selected"; ?>>Minuman</option>
                    </select>                      
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Harga</label>
                    <input type="text" name="harga" class="form-control" value="<?php echo $data->makanan_harga; ?>" required>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Status</label>
                    <select name="status" class="form-control">                      
                        <option value="ada" <?php if($data->makanan_status=='ada') echo "selected"; ?>>Ada</option>
                        <option value="kosong" <?php if($data->makanan_status=='kosong') echo "selected"; ?>>Kosong</option>
                    </select>                      
                </div>
            </div>
        </div>    
    </div>
        
    <div class="modal-footer"> 
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> 
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
<?php echo form_close(); ?>