<script type="text/javascript">
    $(document).ready(function(){

        $("#example1").DataTable({"ordering": false});

        $("#new_karyawan").click(function()
        {
            link = "<?php echo site_url('master/karyawan/insert'); ?>";
            load_modal({size:"modal-lg", header:"Buat Karyawan Baru", url:link});
        });

    	

        $(".edit_karyawan").click(function(){
            //console.log('karyawan');
            id   = $(this).attr("val-id");
            link = "<?php echo site_url('master/karyawan/edit'); ?>/"+id;

            load_modal({size:"modal-lg",header:"Edit Karyawan", url:link});
        });

       $(".delete_karyawan").click(function()
        {
        	name = $(this).attr("val-name");
        	if(confirm("Yakin ingin menghapus karyawan "+name+" ?"))
        	{
        		var location = $(this).attr("href");

				$.post( location, { load_template: "false" } ).done(function( data ) 
				{
                    res=jQuery.parseJSON(data);
                    $("#panel-body").html(res.data);
				});
        	}
        	return false;
        });
    });
</script>
  

<!-- Content Header (Page header) -->
<!--section class="content-header">
    <h1 id="title_header">
        <?php if(isset($title)){echo $title;}else{echo "KARYAWAN";} ?>
    </h1>
</section-->

<?php echo $this->session->flashdata('report'); ?>

<!-- Main content -->
<section class="content">
    <div class="box box-info">
        
        <!--div class="box-header"-->
            <div class="row" style="padding:0px 12px 10px; border-bottom: 1px;" >   
                <h3 class="pull-left" id="title_header" style="margin: 0px;">Karyawan</h3>
                <button class="btn btn-info pull-right" id="new_karyawan"><i class="fa fa-plus"></i> Buat Karyawan Baru</button>
            </div>    
        <!--/div><!-- /.box-header -->

        <div class="box-body">
            <table id="example1" class="table table-bordered table-striped table-hover">
                <thead>
                    <tr>               
                        <th>Nama Karyawan</th>
                        <th>Alamat</th>
                        <th>No. Telepon</th>
                        <th>Golongan</th>
                        <th>Username</th>
                        <!--th>Password</th-->
                        <th>Tanggal Masuk</th>
                        <th>Tanggal Keluar</th>
                        <th width="8%">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        foreach ($list->result() as $row) { 
                     ?>       
                            <tr>
                                <td><?php echo $row->karyawan_name; ?></td>
                                <td><?php echo $row->karyawan_address; ?></td>
                                <td><?php echo $row->karyawan_telp; ?></td>
                                <td><?php echo $row->karyawan_golongan; ?></td>
                                <td><?php echo $row->karyawan_username; ?></td>
                                <!--td><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></td-->
                                <td><?php echo date('d F Y',strtotime($row->date_in)); ?></td>
                                <td><?php echo date('d F Y',strtotime($row->date_out)); ?></td>

                                <td>
                                    <button class="btn btn-xs btn-warning btn-flat edit_karyawan" val-id="<?php echo $row->karyawan_id; ?>"><i class="fa fa-edit"></i></button>      

                                    <a href="<?php echo site_url('master/karyawan/delete').'/'.$row->karyawan_id; ?>" class="delete_karyawan" val-name="<?php echo $row->karyawan_name; ?>">
                                        <button class="btn btn-xs btn-danger btn-flat"><i class="fa fa-trash"></i></button>
                                    </a>                                  
                                </td>
                            </tr>

                           
                        <?php 
                        } // end foreach
                    ?>
                </tbody>    
            </table>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
</section><!-- /.content -->
