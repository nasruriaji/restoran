<script type="text/javascript">
    $(document).ready(function() {
        $("#insertkaryawan").bind('submit', function(event){
            //event.preventDefault();
            var link = $(this).attr('action');
            $.ajax({
                url: link,
                type: "POST",
                data: $(this).serialize(),
                cache: false,
                success: function(respon) {
                    res=jQuery.parseJSON(respon);
                    $("#panel-body").html(res.data);
                    $('#myModal').modal('hide');
                },
                error:function(respon){
                    $('#myModal').modal('hide');
                    //$("#content_body").html(respon);
                }
            });
            return false;
        });
    });
</script>


<?php echo form_open('master/karyawan/post_insert', array('id'=>'insertkaryawan')); ?>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Nama Karyawan</label>
                    <input type="text" name="name" class="form-control" placeholder="Nama Karyawan" required>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Username</label>
                    <input type="text" name="username" class="form-control" placeholder="Username Karyawan" required>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Password</label>
                    <!--input type="text" name="password" class="form-control" placeholder="Password Karyawan" required-->
                    <div class="password">
                        <input type="password" id="passwordfield" name="password" class="form-control" placeholder="Password" required="">
                        <span class="glyphicon glyphicon-eye-open" style="/*display: inline;*/"></span>                      
                    </div>                    
                    <style type="text/css">
                        .password{
                            position: relative;
                        }

                        .password input[type="password"]{
                            padding-right: 30px;
                        }

                        .password .glyphicon,#password2 .glyphicon {
                            display:none;
                            right: 15px;
                            position: absolute;
                            top: 12px;
                            cursor:pointer;
                        }
                    </style>                      
                </div>
            </div>                        
            <div class="col-md-12">
                <div class="form-group">
                    <label>Alamat</label>
                    <input type="text" name="address" class="form-control" placeholder="Alamat Karyawan" required>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Telepon</label>
                    <input type="text" name="telp" class="form-control" placeholder="Nomor Telepon Karyawan" required>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Golongan</label>
                    <select name="golongan" class="form-control">                      
                        <option value="admin">Admin</option>
                        <option value="dapur">Dapur</option>
                        <option value="kasir">Kasir</option>
                        <option value="pelayan">Pelayan</option>
                    </select>                      
                </div>
            </div>
            <script type="text/javascript">
                $(document).ready(function(){
                    //Date picker
                    $('.date_input').datepicker({
                        //dateFormat: "d MM yy",
                        format: "dd MM yyyy",
                        autoclose: true
                    });   
                });    

            </script>              
            <div class="col-md-3">
                <div class="form-group">
                    <label>Tanggal Masuk</label>
                    <div class="input-group">
                        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                        <input type="text" class="form-control pull-right date_input" name="date_in" placeholder="Tanggal Masuk Karyawan" required>
                    </div> 
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Tanggal Keluar</label>
                    <div class="input-group">
                        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                        <input type="text" class="form-control pull-right date_input" name="date_out" placeholder="Tanggal Masuk Karyawan" required>
                    </div> 
                </div>
            </div>

        </div>
    </div>

    <div class="modal-footer"> 
        <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button> 
        <button type="submit" class="btn btn-primary">Simpan</button>
    </div>
<?php echo form_close(); ?>

<script type="text/javascript">

    $("#passwordfield").on("keyup",function(){
        console.log('password');
        if($(this).val())
            $(".glyphicon-eye-open").show();
        else
            $(".glyphicon-eye-open").hide();
    });
    $(".glyphicon-eye-open").mousedown(function(){
                    $("#passwordfield").attr('type','text');
                }).mouseup(function(){
                    $("#passwordfield").attr('type','password');
                }).mouseout(function(){
                    $("#passwordfield").attr('type','password');
    });    
</script>