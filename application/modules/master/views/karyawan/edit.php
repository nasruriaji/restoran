<script>
    $(document).ready(function() 
    {
        $("#editkaryawan").bind('submit', function(event)
        {
            var link = $(this).attr('action');
            $.ajax({
                url: link,
                type: "POST",
                data: $(this).serialize(),
                cache: false,
                success: function(respon) {
                    res=jQuery.parseJSON(respon);
                    $("#panel-body").html(res.data);

                    $('#myModal').modal('hide');
                },
                error:function(respon){
                    $('#myModal').modal('hide');
                }
            });
            return false;
        });
    });
</script>


<?php echo form_open('master/karyawan/post_edit', array('id'=>'editkaryawan')); ?>
    <input type="hidden" name="karyawan_id" value="<?php echo $data->karyawan_id; ?>">
    <div class="modal-body">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Nama Karyawan</label>
                    <input type="text" name="name" class="form-control" value="<?php echo $data->karyawan_name;?>" required>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Username</label>
                    <input type="text" name="username" class="form-control" value="<?php echo $data->karyawan_username;?>" required>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Password</label>
                    <div class="password">
                    <input type="password" id="passwordfield" name="password_baru" class="form-control" value="<?php echo $data->karyawan_password; ?>" required>
                    <input type="hidden" name="password_lama" class="form-control" value="<?php echo $data->karyawan_password; ?>" required>                                
                    <span class="glyphicon glyphicon-eye-open"></span>
                    </div>
                    <style type="text/css">
                        .password{
                            position: relative;
                        }

                        .password input[type="password"]{
                            padding-right: 30px;
                        }

                        .password .glyphicon,#password2 .glyphicon {
                            /*display:none;*/
                            right: 15px;
                            position: absolute;
                            top: 12px;
                            cursor:pointer;
                        }
                    </style>                     
                </div>
            </div>                                    
            <div class="col-md-12">
                <div class="form-group">
                    <label>Alamat</label>
                    <input type="text" name="address" class="form-control" value="<?php echo $data->karyawan_address;?>" required>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Telepon</label>
                    <input type="text" name="telp" class="form-control" value="<?php echo $data->karyawan_telp;?>" required>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Golongan</label>
                    <select name="golongan" class="form-control">                      
                        <option value="admin" <?php if ($data->karyawan_golongan=='admin') echo 'selected'; ?> >Admin</option>
                        <option value="dapur" <?php if ($data->karyawan_golongan=='dapur') echo 'selected'; ?> >Dapur</option>
                        <option value="kasir" <?php if ($data->karyawan_golongan=='kasir') echo 'selected'; ?> >Kasir</option>
                        <option value="pelayan" <?php if ($data->karyawan_golongan=='pelayan') echo 'selected'; ?> >Pelayan</option>
                    </select>                      
                </div>
            </div>
            <script type="text/javascript">
                $(document).ready(function(){
                    //Date picker
                    $('.date_input').datepicker({
                        //dateFormat: "d MM yy",
                        format: "dd MM yyyy",
                        autoclose: true
                    });   
                });    

            </script>              
            <div class="col-md-3">
                <div class="form-group">
                    <label>Tanggal Masuk</label>
                    <div class="input-group">
                        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                        <input type="text" class="form-control pull-right date_input" name="date_in" value="<?php echo date_create($data->date_in)->format('d F Y');?>" required>
                    </div> 
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Tanggal Keluar</label>
                    <div class="input-group">
                        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                        <input type="text" class="form-control pull-right date_input" name="date_out" value="<?php echo date_create($data->date_out)->format('d F Y');?>" required>
                    </div> 
                </div>
            </div>

        </div>    
    </div>
        
    <div class="modal-footer"> 
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> 
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
<?php echo form_close(); ?>

<script type="text/javascript">

    $("#passwordfield").on("keyup",function(){
        console.log('password');
        if($(this).val())
            $(".glyphicon-eye-open").show();
        else
            $(".glyphicon-eye-open").hide();
    });
    $(".glyphicon-eye-open").mousedown(function(){
                    $("#passwordfield").attr('type','text');
                }).mouseup(function(){
                    $("#passwordfield").attr('type','password');
                }).mouseout(function(){
                    $("#passwordfield").attr('type','password');
    });    
</script>