<script>
    $(document).ready(function() 
    {
        $("#editmakanan").bind('submit', function(event)
        {
            var link = $(this).attr('action');
            $.ajax({
                url: link,
                type: "POST",
                data: $(this).serialize(),
                cache: false,
                success: function(respon) {
                    res=jQuery.parseJSON(respon);
                    $("#panel-body").html(res.data);

                    $('#myModal').modal('hide');
                },
                error:function(respon){
                    $('#myModal').modal('hide');
                }
            });
            return false;
        });
    });
</script>


<?php echo form_open('master/pesan/post_edit', array('id'=>'editmakanan')); ?>
    <input type="hidden" name="pesan_id" value="<?php echo $data->pemesanan_id; ?>">
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Nama Makanan</label>
                    <select name="makanan" class="form-control">  
                        <?php 
                            foreach ($makanan->result() as $key) {
                                if ($key->makanan_id==$data->makanan_id)
                                    echo "<option value='".$key->makanan_id."' selected>".$key->makanan_name."</option>";
                                else
                                    echo "<option value='".$key->makanan_id."'>".$key->makanan_name."</option>";
                            }
                            
                         ?>           
                    </select>  
                </div>
            </div>        
            <div class="col-md-12">
                <div class="form-group">
                    <label>Jumlah Pemesanan</label>
                    <input type="text" name="jumlah" class="form-control" value="<?php echo $data->pemesanan_jum; ?>" required>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label>Keterangan</label>
                    <textarea name="keterangan" class="form-control" style="height: 100px;"> <?php echo $data->pemesanan_ket; ?> </textarea>
                    <!--input type="text" name="keterangan" class="form-control" placeholder="Keterangan Pemesanan" required-->
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label>Meja</label>
                    <!--input type="text" name="meja" class="form-control" value="<?php echo $data->pemesanan_meja; ?>" required-->
                    <select name="meja" class="form-control">  
                        <?php for ($x = 1; $x <= 20; $x++) { 
                            if ($data->pemesanan_meja==$x)
                                echo "<option value='".$x."' selected >".$x."</option>";
                            else
                                echo "<option value='".$x."'  >".$x."</option>";
                        } ?>           
                    </select>                       
                </div>
            </div>            

        </div>    
    </div>
        
    <div class="modal-footer"> 
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> 
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
<?php echo form_close(); ?>