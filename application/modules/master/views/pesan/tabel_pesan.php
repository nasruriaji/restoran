            <table id="example1" class="table table-bordered table-striped table-hover">
                <thead>
                    <tr>             
                        <th>Nama Makanan</th>  
                        <th>Jumlah Pemesanan</th>
                        <th>Keterangan</th>
                        <th>Meja</th>
                        
                        <th width="8%">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        foreach ($list->result() as $row) { 
                     ?>       
                            <tr>
                                <td><?php echo $row->makanan_name; ?></td>
                                <td><?php echo $row->pemesanan_jum; ?></td>
                                <td><?php echo $row->pemesanan_ket; ?></td>
                                <td><?php echo $row->pemesanan_meja; ?></td>

                                <td>
                                    <button class="btn btn-xs btn-warning btn-flat edit_pesan" val-id="<?php echo $row->pemesanan_id; ?>"><i class="fa fa-edit"></i></button>    

                                    <!--a href="<?php //echo site_url('master/makanan/delete').'/'.$row->makanan_id; ?>" class="delete_makanan" val-name="<?php echo $row->makanan_name; ?>">
                                        <button class="btn btn-xs btn-danger btn-flat"><i class="fa fa-trash"></i></button>
                                    </a-->                                  
                                </td>
                            </tr>

                           
                        <?php 
                        } // end foreach
                    ?>
                </tbody>    
            </table>