<script type="text/javascript">
    $(document).ready(function() {
        $("#insertpesan").bind('submit', function(event){
            //event.preventDefault();
            var link = $(this).attr('action');
            $.ajax({
                url: link,
                type: "POST",
                data: $(this).serialize(),
                cache: false,
                success: function(respon) {
                    res=jQuery.parseJSON(respon);
                    $("#panel-body").html(res.data);
                    $('#myModal').modal('hide');
                },
                error:function(respon){
                    $('#myModal').modal('hide');
                }
            });
            return false;
        });
    });
</script>

<?php echo form_open('master/pesan/post_insert', array('id'=>'insertpesan')); ?>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Nama Makanan</label>
                    <select name="makanan" class="form-control">  
                        <?php foreach ($makanan->result() as $key) {
                            echo "<option value='".$key->makanan_id."'>".$key->makanan_name."</option>";
                        } ?>           
                    </select>  
                </div>
            </div>        
            <div class="col-md-12">
                <div class="form-group">
                    <label>Jumlah Pemesanan</label>
                    <input type="text" name="jumlah" class="form-control" placeholder="Jumlah Pemesanan" required>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label>Keterangan</label>
                    <textarea name="keterangan" class="form-control" placeholder="Keterangan Pemesanan" style="height: 100px;"></textarea>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label>Meja</label>
                    <!--input type="text" name="meja" class="form-control" placeholder="Meja Pemesanan" required-->
                    <select name="meja" class="form-control">  
                        <?php for ($x = 1; $x <= 20; $x++) { 
                            echo "<option value='".$x."'>".$x."</option>";
                        } ?>           
                    </select>                      
                </div>
            </div>            

        </div>
    </div>

    <div class="modal-footer"> 
        <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button> 
        <button type="submit" class="btn btn-primary">Simpan</button>
    </div>
<?php echo form_close(); ?>