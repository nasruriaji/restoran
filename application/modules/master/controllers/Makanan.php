<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class Makanan extends MX_Controller
{
	
	function __construct()
	{
		parent::__construct();
        $this->load->model('m_makanan');
	}

    public function index() {
        $data["list"]         = $this->m_makanan->getdata("");   
        $data["title"]        = "MAKANAN";
        
        $temp['data']=$this->load->view("makanan/home", $data,TRUE);
        
        echo json_encode($temp);        
    } 

    public function insert(){
        $this->load->view("master/makanan/insert");
    }  

    public function post_insert(){
        // insert table makanan
        $data_insert    = array(
                            "makanan_name"=>$this->input->post("name"),
                            "makanan_jenis"=>$this->input->post("jenis"),
                            "makanan_harga"=>$this->input->post("harga"),
                            "makanan_status"=>$this->input->post("status"),

                            "karyawan_id_created"=>$this->session->userdata('user_id'),
                            "date_created"=>date("Y-m-d H:i:s")
                        );
        $id_admin = $this->m_makanan->insert($data_insert);

        $this->session->set_flashdata('report', valid("Berhasil Menambah data makanan"));
        $data["list"]         = $this->m_makanan->getdata();   
        $data["title"]        = "MAKANAN";
        
        $temp['data']=$this->load->view("makanan/home", $data,TRUE);
        echo json_encode($temp);          
       
    }   
   public function edit($id=""){
        $data["data"] = $this->m_makanan->getdata($id);
        if($data["data"]->num_rows()==1)
        { 
            $data["data"] = $data["data"]->row();

            $data["title"]  = "EDIT MAKANAN";
            $this->load->view("makanan/edit", $data);           
        }
    }

    public function post_edit(){
        $data_update    = array(
                            "makanan_name"=>$this->input->post("name"),
                            "makanan_jenis"=>$this->input->post("jenis"),
                            "makanan_harga"=>$this->input->post("harga"),
                            "makanan_status"=>$this->input->post("status"),

                            "karyawan_id_updated"=>$this->session->userdata('user_id'),
                            "date_updated"=>date("Y-m-d H:i:s")
                        );
        $this->m_makanan->update($data_update,array("makanan_id"=>$this->input->post("makanan_id")));

        $this->session->set_flashdata('report', valid("Berhasil edit data makanan"));

        $data["list"]         = $this->m_makanan->getdata();  
        $data["title"]        = "MAKANAN";
        
        $temp['data']=$this->load->view("makanan/home", $data,TRUE);
        echo json_encode($temp); 
    }
   public function delete($id=''){
        $cek = $this->m_makanan->getdata($id)->num_rows();
        if($cek == 1){
            $this->db->update('makanan', array('makanan_status' => 'N','date_updated' => date("Y-m-d H:i:s"), 'karyawan_id_updated'=>$this->session->userdata('user_id')),array('makanan_id' => $id));
                
            $this->session->set_flashdata('report', valid("Berhasil hapus data makanan"));
        }       

        $data["list"]         = $this->m_makanan->getdata("");  
        $data["title"]        = "MAKANAN";
        
        $temp['data']=$this->load->view("makanan/home", $data,TRUE);
        echo json_encode($temp);
    } 
}    