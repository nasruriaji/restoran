<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class Karyawan extends MX_Controller
{
	
	function __construct()
	{
		parent::__construct();
        $this->load->model('m_karyawan');
	}

    public function index() {
        $data["list"]         = $this->m_karyawan->getdata("");   
        $data["title"]        = "KARYAWAN";
        
        $temp['data']=$this->load->view("karyawan/home", $data,TRUE);
        
        echo json_encode($temp);     
    } 

    public function insert(){
        $this->load->view("master/karyawan/insert");
    }  

    public function post_insert(){
        // insert table karyawan
        $data_insert    = array(
                            "karyawan_name"=>$this->input->post("name"),
                            "karyawan_address"=>$this->input->post("address"),
                            "karyawan_telp"=>$this->input->post("telp"),
                            "karyawan_golongan"=>$this->input->post("golongan"),
                            "karyawan_username"=>$this->input->post("username"),
                            "karyawan_password"=>md5($this->input->post("password")),
                            "date_in"=> date('Y-m-d',strtotime($this->input->post("date_in"))),
                            "date_out"=>date('Y-m-d',strtotime($this->input->post("date_out"))),

                            "karyawan_id_created"=>$this->session->userdata('user_id'),
                            "date_created"=>date("Y-m-d H:i:s")
                        );
        $id_admin = $this->m_karyawan->insert($data_insert);

        $this->session->set_flashdata('report', valid("Berhasil Menambah data karyawan"));
        $data["list"]         = $this->m_karyawan->getdata();   
        $data["title"]        = "KARYAWAN";
        
        $temp['data']=$this->load->view("karyawan/home", $data,TRUE);
        echo json_encode($temp);          
       
    }   
   public function edit($id=""){
        $data["data"] = $this->m_karyawan->getdata($id);
        if($data["data"]->num_rows()==1)
        { 
            $data["data"] = $data["data"]->row();

            $data["title"]  = "EDIT KARYAWAN";
            $this->load->view("karyawan/edit", $data);           
        }
    }

    public function post_edit(){
        $password_baru=$this->input->post('password_baru');
        $password_lama=$this->input->post('password_lama');

        if ($password_baru==$password_lama){
            $data_update    = array(
                                "karyawan_name"=>$this->input->post("name"),
                                "karyawan_address"=>$this->input->post("address"),
                                "karyawan_telp"=>$this->input->post("telp"),
                                "karyawan_golongan"=>$this->input->post("golongan"),
                                "karyawan_username"=>$this->input->post("username"),

                                "date_in"=> date('Y-m-d',strtotime($this->input->post("date_in"))),
                                "date_out"=>date('Y-m-d',strtotime($this->input->post("date_out"))),

                                "karyawan_id_updated"=>'1',//$this->session->userdata('user_id'),
                                "date_updated"=>date("Y-m-d H:i:s")
                            );
        }
        else if ($password_baru!=$password_lama){
            $data_update    = array(
                                "karyawan_name"=>$this->input->post("name"),
                                "karyawan_address"=>$this->input->post("address"),
                                "karyawan_telp"=>$this->input->post("telp"),
                                "karyawan_golongan"=>$this->input->post("golongan"),
                                "karyawan_username"=>$this->input->post("username"),
                                "karyawan_password"=> md5($password_baru),

                                "date_in"=> date('Y-m-d',strtotime($this->input->post("date_in"))),
                                "date_out"=>date('Y-m-d',strtotime($this->input->post("date_out"))),

                                "karyawan_id_updated"=>$this->session->userdata('user_id'),
                                "date_updated"=>date("Y-m-d H:i:s")
                            );
        }    


        $this->m_karyawan->update($data_update,array("karyawan_id"=>$this->input->post("karyawan_id")));

        $this->session->set_flashdata('report', valid("Berhasil edit data karyawan"));

        $data["list"]         = $this->m_karyawan->getdata();  
        $data["title"]        = "KARYAWAN";
        
        $temp['data']=$this->load->view("karyawan/home", $data,TRUE);
        echo json_encode($temp); 
    }
   public function delete($id=''){
        $cek = $this->m_karyawan->getdata($id)->num_rows();
        if($cek == 1){
            $this->db->update('karyawan', array('karyawan_status' => 'N','date_updated' => date("Y-m-d H:i:s"), 'karyawan_id_updated'=>$this->session->userdata('user_id')),array('karyawan_id' => $id));
                
            $this->session->set_flashdata('report', valid("Berhasil hapus data karyawan"));
        }       

        $data["list"]         = $this->m_karyawan->getdata("");  
        $data["title"]        = "KARYAWAN";
        
        $temp['data']=$this->load->view("karyawan/home", $data,TRUE);
        echo json_encode($temp);
    } 
}    