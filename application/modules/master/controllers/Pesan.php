<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class Pesan extends MX_Controller
{
	
	function __construct(){
		parent::__construct();
        $this->load->model('m_pesan');
	}

    public function index() {
        $data["list"]         = $this->m_pesan->getdata("");   
        $data["title"]        = "PEMESANAN";
        
        $temp['data']=$this->load->view("pesan/home", $data,TRUE);
        
        echo json_encode($temp);        
    } 

    public function tabel_pesan() {
        $data["list"]         = $this->m_pesan->getdata("");   
        $data["title"]        = "PEMESANAN";
        
        $temp['data']=$this->load->view("pesan/tabel_pesan", $data,TRUE);
        
        echo json_encode($temp);        
    }     

    public function insert(){
        $data["makanan"]         = $this->m_pesan->getmakanan();
        $this->load->view("master/pesan/insert",$data);
    }  

    public function post_insert(){
        // insert table pemesanan
        $data_insert    = array(
                            "pemesanan_jum"=>$this->input->post("jumlah"),
                            "pemesanan_ket"=>$this->input->post("keterangan"),
                            "pemesanan_meja"=>$this->input->post("meja"),
                            "makanan_id"=>$this->input->post("makanan"),

                            "karyawan_id_created"=>$this->session->userdata('user_id'),
                            "date_created"=>date("Y-m-d H:i:s")
                        );
        $id_admin = $this->m_pesan->insert($data_insert);

        $this->session->set_flashdata('report', valid("Berhasil Menambah data pemesanan"));
        $data["list"]         = $this->m_pesan->getdata();   

        $data["title"]        = "PEMESANAN";
        
        $temp['data']=$this->load->view("pesan/home", $data,TRUE);
        echo json_encode($temp);          
       
    }   
   public function edit($id=""){    

        $data["data"]            = $this->m_pesan->getdata($id);
        //die(if($data['data']));
        $data["makanan"]         = $this->m_pesan->getmakanan();
        if($data["data"]->num_rows()==1)
        { 
            $data["data"] = $data["data"]->row();

            $data["title"]  = "EDIT PEMESANAN";
            $this->load->view("pesan/edit", $data);           
        }
    }

    public function post_edit(){
        $data_update    = array(
                            "pemesanan_jum"=>$this->input->post("jumlah"),
                            "pemesanan_ket"=>$this->input->post("keterangan"),
                            "pemesanan_meja"=>$this->input->post("meja"),
                            "makanan_id"=>$this->input->post("makanan"),

                            "karyawan_id_updated"=>$this->session->userdata('user_id'),
                            "date_updated"=>date("Y-m-d H:i:s")
                        );
        $this->m_pesan->update($data_update,array("pemesanan_id"=>$this->input->post("pesan_id")));

        $this->session->set_flashdata('report', valid("Berhasil edit data pemesanan"));

        $data["list"]         = $this->m_pesan->getdata();  
        $data["title"]        = "PEMESANAN";
        
        $temp['data']=$this->load->view("pesan/home", $data,TRUE);
        echo json_encode($temp); 
    }
   public function delete($id=''){
        $cek = $this->m_pesan->getdata($id)->num_rows();
        if($cek == 1){
            $this->db->update('pemesanan', array('pemesanan_status' => 'N','date_updated' => date("Y-m-d H:i:s"), 'karyawan_id_updated'=>$this->session->userdata('user_id')),array('pemesanan_id' => $id)); 
                
            $this->session->set_flashdata('report', valid("Berhasil hapus data pemesanan"));
        }       

        $data["list"]         = $this->m_pesan->getdata("");  
        $data["title"]        = "PEMESANAN";
        
        $temp['data']=$this->load->view("pesan/home", $data,TRUE);
        echo json_encode($temp);
    } 
}    