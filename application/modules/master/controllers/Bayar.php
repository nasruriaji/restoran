<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class Bayar extends MX_Controller
{
	
	function __construct()
	{
		parent::__construct();
        $this->load->model('m_bayar');
	}

    public function index() {
        $data["list"]         = $this->m_bayar->getdata("");   
        $data["title"]        = "BAYAR";
        
        $temp['data']=$this->load->view("bayar/home", $data,TRUE);
        
        echo json_encode($temp);        
    } 

    public function insert(){
        $this->load->view("master/bayar/insert");
    }  

    public function post_insert(){
        // insert table pembayaran
        $pesan_id=$this->input->post("pesan_id");
        $result=$this->m_bayar->getdatapesan($pesan_id);


        $data_insert    = array(
                            "pembayaran_total"=>$result->row()->total,
                            "pemesanan_id"=>$result->row()->pemesanan_id,

                            "karyawan_id_created"=>$this->session->userdata('user_id'),
                            "date_created"=>date("Y-m-d H:i:s")
                        );
        $id_admin = $this->m_bayar->insert($data_insert);

        $this->session->set_flashdata('report', valid("Berhasil Menambah data pembayaran"));
        $data["list"]         = $this->m_bayar->getdata();   
        $data["title"]        = "PEMBAYARAN";
        
        $temp['data']=$this->load->view("bayar/home", $data,TRUE);
        echo json_encode($temp);
    }   
   public function edit($id=""){
        $data["data"] = $this->m_bayar->getdata($id);
        if($data["data"]->num_rows()==1) { 
            $data["data"] = $data["data"]->row();

            $data["title"]  = "EDIT PEMBAYARAN";
            $this->load->view("bayar/edit", $data);           
        }
    }

    public function post_edit(){
        $data_update    = array(
                            "pembayaran_total"=>$this->input->post("total"),
                            "pemesanan_id"=>$this->input->post("pesan"),

                            "karyawan_id_updated"=>$this->session->userdata('user_id'),
                            "date_updated"=>date("Y-m-d H:i:s")
                        );
        $this->m_bayar->update($data_update,array("pembayaran_id"=>$this->input->post("bayar_id")));

        $this->session->set_flashdata('report', valid("Pesanan sudah terbayar"));

        $data["list"]         = $this->m_bayar->getdata();  
        $data["title"]        = "PEMBAYARAN";
        
        $temp['data']=$this->load->view("bayar/home", $data,TRUE);
        echo json_encode($temp); 
    }
}    