<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_pesan extends CI_Model
{
	function __construct(){
        parent::__construct();
    }

    function getdata($id=""){  
        if ($id!=""){
            $query="
                SELECT p.*,m.makanan_name 
                FROM pemesanan p
                LEFT JOIN makanan m ON p.makanan_id=m.makanan_id
                WHERE p.pemesanan_id='".$id."'";
        }else{            
            $query="
                SELECT p.*,m.makanan_name 
                FROM pemesanan p
                LEFT JOIN makanan m ON p.makanan_id=m.makanan_id
                where p.pemesanan_id not in (select pe.pemesanan_id from pembayaran pe)
            ";
        }        
        $hasil = $this->db->query($query);  
        return $hasil;         
    }

    function insert($datainsert=array()){
        $this->db->insert('pemesanan', $datainsert);
        return $this->db->insert_id();
    }

    function update($data=array(),$where=array()){
        $this->db->update('pemesanan', $data, $where);
    }

    function getmakanan(){  
        $hasil = $this->db->query("select * from makanan where makanan_status='ada'");  
        return $hasil;         
    }    
}
?>