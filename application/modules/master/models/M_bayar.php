<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_bayar extends CI_Model
{
	function __construct(){
        parent::__construct();
    }

    function getdata($id=""){  
        if ($id!=""){
            $query="select * from pembayaran where pembayaran_id='".$id."'";
        }else{            
            $query="
                    SELECT p.pemesanan_id,p.makanan_id, m.makanan_name, p.pemesanan_meja,p.pemesanan_jum,m.makanan_harga, CAST( p.pemesanan_jum AS INTEGER)* CAST(m.makanan_harga AS INTEGER) total, 0 AS status
                    FROM pemesanan p
                    LEFT JOIN makanan m ON m.makanan_id=p.makanan_id
                    WHERE pemesanan_id NOT IN (SELECT pemesanan_id FROM pembayaran)
                    UNION ALL
                    SELECT p.pemesanan_id,p.makanan_id, m.makanan_name, p.pemesanan_meja,p.pemesanan_jum,m.makanan_harga, CAST( p.pemesanan_jum AS INTEGER)* CAST(m.makanan_harga AS INTEGER) total, 1 AS status
                    FROM pemesanan p
                    LEFT JOIN makanan m ON m.makanan_id=p.makanan_id
                    WHERE pemesanan_id IN (SELECT pemesanan_id FROM pembayaran)
                    ORDER BY pemesanan_id

             ";
        }        
        $hasil = $this->db->query($query);  
        return $hasil;         
    }

    function getdatapesan($id=""){
        return $this->db->query("
                    SELECT p.pemesanan_id,p.makanan_id, m.makanan_name, p.pemesanan_meja,p.pemesanan_jum,m.makanan_harga, CAST( p.pemesanan_jum AS INTEGER)* CAST(m.makanan_harga AS INTEGER) total, 0 AS STATUS
                    FROM pemesanan p
                    LEFT JOIN makanan m ON m.makanan_id=p.makanan_id
                    WHERE pemesanan_id NOT IN (SELECT pemesanan_id FROM pembayaran) and pemesanan_id='".$id."'
            "   );
    }  


    function insert($datainsert=array()){
        $this->db->insert('pembayaran', $datainsert);
        return $this->db->insert_id();
    }

    function update($data=array(),$where=array()){
        $this->db->update('pembayaran', $data, $where);
    }
}
?>