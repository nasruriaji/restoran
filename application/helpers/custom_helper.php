<?php 



// ini untuk contoh format inputan 2017-07-28 19:50:50
function format_tanggal_indonesia($tanggal) {
    $bulan = array(
        '01' => 'Januari',
        '02' => 'Februari',
        '03' => 'Maret',
        '04' => 'April',
        '05' => 'Mei',
        '06' => 'Juni',
        '07' => 'Juli',
        '08' => 'Agustus',
        '09' => 'September',
        '10' => 'Oktober',
        '11' => 'November',
        '12' => 'Desember'
    );

    // pecah string
    $tanggalan = explode('-', $tanggal);
    return substr($tanggalan[2],0,2) . ' ' . $bulan[$tanggalan[1]] . ' ' . $tanggalan[0];
}

// ini untuk mem parse ke format rupiah
function format_rupiah($nilai = 0) {
    return 'Rp ' . number_format($nilai, 0, '.', '.');
}

?>