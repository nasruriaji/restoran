<!DOCTYPE html>
<html class='no-js' lang='en'>
  <head>
    <meta charset='utf-8'>
    <meta content='IE=edge,chrome=1' http-equiv='X-UA-Compatible'>
    <title>Dashboard</title>
    <meta content='lab2023' name='author'>
    <meta content='' name='description'>
    <meta content='' name='keywords'>

      
    
    <link href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>" >  
    <link href="<?php echo base_url('assets/css/bootstrap-switch.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/application-a07755f5.css'); ?> " rel="stylesheet" type="text/css" />

    <link rel="stylesheet" href="<?php echo base_url('assets/css/font-awesome.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/dataTables.bootstrap.min.css'); ?>" >
    <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap-datepicker.min.css'); ?>">
    <link rel="stylesheet" href="<?php //echo base_url('assets/css/AdminLTE.min.css'); ?>">



    <script src="<?php echo base_url('assets/js/jquery-3.2.1.min.js'); ?> "> </script>
    <script src="<?php echo base_url('assets/js/tether.min.js'); ?> "> </script> 
    <script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?> "> </script> 
    <script src="<?php echo base_url('assets/js/bootstrap-switch.min.js'); ?> "> </script> 
    <script src="<?php echo base_url('assets/js/custom.js'); ?> "> </script> 
    <script src="<?php echo base_url('assets/js/datatables/jquery.dataTables.min.js'); ?> "> </script>
    <script src="<?php echo base_url('assets/js/datatables/dataTables.bootstrap.min.js'); ?> "> </script> 
    <script src="<?php echo base_url('assets/js/application-985b892b.js'); ?> " type="text/javascript"></script>
    <script src="<?php echo base_url('assets/js/bootstrap-datepicker.min.js'); ?>"></script>
           

    <style type="text/css">
        .btn, .btn-xs{
            padding: 4px 7px;
        }
        .datepicker{
          background-color: #f7fbff;
        }
    </style>



    <link href="assets/images/favicon.ico" rel="icon" type="image/ico" />
    
  </head>
  <body class='main page'>
    <!-- Navbar -->
    <div class='navbar navbar-default' id='navbar'>
      <a class='navbar-brand' href='#'>
        <i class='icon-beer'></i>
        Hierapolis
      </a>
      <ul class='nav navbar-nav pull-right'>
        <li class='dropdown'>
          <a class='dropdown-toggle' data-toggle='dropdown' href='#'>
            <i class='icon-envelope'></i>
            Messages
            <span class='badge'>5</span>
            <b class='caret'></b>
          </a>
          <ul class='dropdown-menu'>
            <li>
              <a href='#'>New message</a>
            </li>
            <li>
              <a href='#'>Inbox</a>
            </li>
            <li>
              <a href='#'>Out box</a>
            </li>
            <li>
              <a href='#'>Trash</a>
            </li>
          </ul>
        </li>
        <li>
          <a href='#'>
            <i class='icon-cog'></i>
            Settings
          </a>
        </li>
        <li class='dropdown user'>
          <a class='dropdown-toggle' data-toggle='dropdown' href='#'>
            <i class='icon-user'></i>
            <strong><?php echo $this->session->userdata('user_name');?></strong>
            <img class="img-rounded" src="http://placehold.it/20x20/ccc/777" />
            <b class='caret'></b>
          </a>
          <ul class='dropdown-menu'>
            <li>
              <a href='#'>Edit Profile</a>
            </li>
            <li class='divider'></li>
            <li>
              <a href="<?php echo base_url('login/logout'); ?>">Sign out</a>
            </li>
          </ul>
        </li>
      </ul>
    </div>
    <div id='wrapper'>
      <!-- Sidebar -->
    
    <!--script type='text/javascript'> var $jq = jQuery.noConflict();  </script-->      
    <script type="text/javascript">
            
        $(document).ready(function(){
          //alert('=============');
            $('#karyawan').click(function(e){
                e.preventDefault();
                var location_cari="<?php echo site_url('master/karyawan')?>";

                $.post( location_cari, 
                    { data: "" },
                    function( data ) {
                        res=jQuery.parseJSON(data);

                        $("#panel-body").html(res.data);
                        
                    });     
            });

            $('#makanan').click(function(e){
                e.preventDefault();
                var location_cari="<?php echo site_url('master/makanan')?>";

                $.post( location_cari, 
                    { data: "" },
                    function( data ) {
                        res=jQuery.parseJSON(data);
                        $("#panel-body").html(res.data);
                    });     
            }); 

            $('#pesan').click(function(e){
                e.preventDefault();
                var location_cari="<?php echo site_url('master/pesan')?>";

                $.post( location_cari, 
                    { data: "" },
                    function( data ) {
                        res=jQuery.parseJSON(data);
                        $("#panel-body").html(res.data);
                    });     
            });  
            $('#bayar').click(function(e){
                e.preventDefault();
                var location_cari="<?php echo site_url('master/bayar')?>";

                $.post( location_cari, 
                    { data: "" },
                    function( data ) {
                        res=jQuery.parseJSON(data);
                        $("#panel-body").html(res.data);
                    });     
            });                                   
        });    

    <?php $access_code=$this->session->userdata('access_code'); ?>
    </script>      
      <section id='sidebar'>
        <i class='icon-align-justify icon-large' id='toggle'></i>
        <ul id='dock'>
          <li class='active launcher'><i class='fa fa-dashboard'></i><a href="">Dashboard</a></li>
          <?php if (($access_code=='admin')or ($access_code=='dapur')){ ?> 
            <li class='launcher dropdown hover'>
              <i class='fa fa-desktop'></i>
              <a href=''>Master Data</a>
              <ul class='dropdown-menu'>
                <li class='dropdown-header'>Master Data</li>
                <li>
                  <a id="karyawan" href=''>Karyawan</a>
                </li>
                <li>
                  <a id='makanan' href=''>Menu Makanan</a>
                </li>
              </ul>
            </li>    
          <?php } ?>
          <?php if (($access_code=='kasir')or($access_code=='pelayan')){ ?>      
            <li class='launcher' id="pesan"><i class='glyphicon glyphicon-cutlery'></i><a href="">Pemesanan</a></li>
          <?php } ?>  
          <?php if ($access_code=='kasir'){ ?> 
            <li class='launcher' id="bayar"><i class='fa fa-usd'></i><a href="">Pembayaran</a></li>
          <?php } ?>
        </ul>
        <div data-toggle='tooltip' id='beaker' title='Made by lab2023'></div>
      </section>
      <!-- Tools -->
      <section id='tools'>
        <ul class='breadcrumb' id='breadcrumb'>
          <li class='title'>Dashboard</li>
          <li><a href="#">Lorem</a></li>
          <li class='active'><a href="#">ipsum</a></li>
        </ul>
        <div id='toolbar'>
          <div class='btn-group'>
            <a class='btn' data-toggle='toolbar-tooltip' href='#' title='Building'>
              <i class='fa fa-building-o'></i>
            </a>
            <a class='btn' data-toggle='toolbar-tooltip' href='#' title='Laptop'>
              <i class='fa fa-laptop'></i>
            </a>
            <a class='btn' data-toggle='toolbar-tooltip' href='#' title='Calendar'>
              <i class='icon-calendar'></i>
              <span class='badge'>3</span>
            </a>
            <a class='btn' data-toggle='toolbar-tooltip' href='#' title='Lemon'>
              <i class='fa fa-lemon-o'></i>
            </a>
          </div>
          <div class='label label-danger'>
            Danger
          </div>
          <div class='label label-info'>
            Info
          </div>
        </div>
      </section>
      <!-- Content -->
      <div id='content'>
        <div class='panel panel-default'>
          <div class='panel-heading'>
            <i class='icon-beer icon-large'></i>
            Hierapolis Rocks
            <div class='panel-tools'>
              <div class='btn-group'>
                <a class='btn' href='#'>
                  <i class='icon-refresh'></i>
                  Refresh statics
                </a>
                <a class='btn' data-toggle='toolbar-tooltip' href='#' title='Toggle'>
                  <i class='icon-chevron-down'></i>
                </a>
              </div>
            </div>
          </div>

          <div id='panel-body' class="panel-body">













          </div>
        </div>
      </div>
    </div>
    <!-- Footer -->


    <!-- Google Analytics -->
    <script>
      var _gaq=[['_setAccount','UA-XXXXX-X'],['_trackPageview']];
      (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
      g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
      s.parentNode.insertBefore(g,s)}(document,'script'));
    </script>
  </body>
</html>

