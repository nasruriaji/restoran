/*
SQLyog Professional v12.09 (64 bit)
MySQL - 10.1.21-MariaDB : Database - restoran
*********************************************************************
*/


/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`restoran` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `restoran`;

/*Table structure for table `karyawan` */

DROP TABLE IF EXISTS `karyawan`;

CREATE TABLE `karyawan` (
  `karyawan_id` int(15) NOT NULL AUTO_INCREMENT,
  `karyawan_name` varchar(50) DEFAULT NULL,
  `karyawan_address` varchar(255) DEFAULT NULL,
  `karyawan_telp` varchar(20) DEFAULT NULL,
  `karyawan_golongan` varchar(10) DEFAULT NULL,
  `date_in` date DEFAULT NULL,
  `date_out` date DEFAULT NULL,
  `karyawan_status` varchar(2) DEFAULT 'Y',
  `karyawan_username` varchar(15) DEFAULT NULL,
  `karyawan_password` varchar(255) DEFAULT NULL,
  `date_created` date DEFAULT NULL,
  `date_updated` date DEFAULT NULL,
  `karyawan_id_created` int(15) DEFAULT NULL,
  `karyawan_id_updated` int(15) DEFAULT NULL,
  PRIMARY KEY (`karyawan_id`),
  UNIQUE KEY `karyawan_username` (`karyawan_username`)
) ENGINE=Aria AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

/*Table structure for table `makanan` */

DROP TABLE IF EXISTS `makanan`;

CREATE TABLE `makanan` (
  `makanan_id` int(11) NOT NULL AUTO_INCREMENT,
  `makanan_name` varchar(50) DEFAULT NULL,
  `makanan_jenis` varchar(10) DEFAULT NULL,
  `makanan_harga` int(10) DEFAULT NULL,
  `makanan_status` varchar(10) DEFAULT NULL,
  `date_created` date DEFAULT NULL,
  `date_updated` date DEFAULT NULL,
  `karyawan_id_created` int(11) DEFAULT NULL,
  `karyawan_id_updated` int(11) DEFAULT NULL,
  PRIMARY KEY (`makanan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

/*Table structure for table `pembayaran` */

DROP TABLE IF EXISTS `pembayaran`;

CREATE TABLE `pembayaran` (
  `pembayaran_id` int(11) NOT NULL AUTO_INCREMENT,
  `pembayaran_total` int(11) DEFAULT NULL,
  `pemesanan_id` int(10) DEFAULT NULL,
  `karyawan_id_created` int(10) DEFAULT NULL,
  `karyawan_id_updated` int(10) DEFAULT NULL,
  `date_created` date DEFAULT NULL,
  `date_updated` date DEFAULT NULL,
  PRIMARY KEY (`pembayaran_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

/*Table structure for table `pemesanan` */

DROP TABLE IF EXISTS `pemesanan`;

CREATE TABLE `pemesanan` (
  `pemesanan_id` int(10) NOT NULL AUTO_INCREMENT,
  `pemesanan_jum` int(5) DEFAULT NULL,
  `pemesanan_ket` varchar(255) DEFAULT NULL,
  `pemesanan_meja` varchar(5) DEFAULT NULL,
  `pemesanan_status` varchar(5) DEFAULT 'Y',
  `makanan_id` int(11) DEFAULT NULL,
  `karyawan_id_created` int(10) DEFAULT NULL,
  `karyawan_id_updated` int(10) DEFAULT NULL,
  `date_created` date DEFAULT NULL,
  `date_updated` date DEFAULT NULL,
  PRIMARY KEY (`pemesanan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
